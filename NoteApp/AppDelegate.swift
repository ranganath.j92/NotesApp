//
//  AppDelegate.swift
//  NoteApp
//
//  Created by apple on 4/12/18.
//  Copyright © 2018 Vibrant. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.makeKeyAndVisible()
        
        
        let service = NoteService()
        let sceneCoordinator = SceneCoordinator(window: window!)
        
        let notesViewModel = NotesViewModel(noteService: service, coordinator: sceneCoordinator)
        let firstScene = Scene.notes(notesViewModel)
        sceneCoordinator.transition(to: firstScene, type: .root)
        return true
    }

}

