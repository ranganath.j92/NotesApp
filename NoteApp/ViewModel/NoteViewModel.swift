//
//  NoteViewModel.swift
//  NoteApp
//
//  Created by apple on 4/12/18.
//  Copyright © 2018 Vibrant. All rights reserved.
//


import Foundation
import RxSwift
import RxDataSources
import Action

typealias NoteSection = AnimatableSectionModel<String, NoteItem>

struct NotesViewModel {
    let sceneCoordinator: SceneCoordinatorType
    let noteService: NoteServiceType
    
//    lazy var statistics: Observable<NoteStatistics> = self.noteService.statistics()
    
    init(noteService: NoteServiceType, coordinator: SceneCoordinatorType) {
        self.noteService = noteService
        self.sceneCoordinator = coordinator
    }
    
    
    func onDelete(note: NoteItem) -> CocoaAction {
        return CocoaAction {
            return self.noteService.delete(note: note)
        }
    }
    
    func onUpdate(note: NoteItem) -> Action<(String,String), Void> {
        return Action { (newTitle,newDesc) in
            return self.noteService.update(note: note, title:newTitle , desc: newDesc).map{ _ in}
        }
    }
    
    var sectionedItems: Observable<[NoteSection]> {
        return self.noteService.notes()
            .map { results in
                let notes = results
                    .sorted(byKeyPath: "added", ascending: false)
                return [
                    NoteSection(model: "Notes", items: notes.toArray())
                ]
        }
    }
    
    func onCreateNote() -> CocoaAction {
        return CocoaAction { _ in
            return self.noteService
                .createNote(title: "", desc: "")
                .flatMap { note -> Observable<Void> in
                    let editViewModel = EditNoteViewModel(note: note,
                                                          coordinator: self.sceneCoordinator,
                                                          updateAction: self.onUpdate(note: note),
                                                          cancelAction: self.onDelete(note: note))
                    return self.sceneCoordinator
                        .transition(to: Scene.editNote(editViewModel), type: .modal)
                        .asObservable().map { _ in }
            }
        }
    }
    
    lazy var editAction: Action<NoteItem, Swift.Never> = { this in
        return Action { note in
            let editViewModel =
                EditNoteViewModel(
                note: note,
                coordinator: this.sceneCoordinator,
                updateAction: this.onUpdate(note: note)
            )
            return this.sceneCoordinator
                .transition(to: Scene.editNote(editViewModel), type: .modal)
                .asObservable()
        }
    }(self)
    
    lazy var readAction: Action<NoteItem, Swift.Never> = { this in
        return Action { note in
            let editViewModel = EditNoteViewModel(note: note, coordinator: this.sceneCoordinator, updateAction: this.onUpdate(note: note), cancelAction: nil, readOnly: true)
            return this.sceneCoordinator
                .transition(to: Scene.editNote(editViewModel), type: .push)
                .asObservable()
        }
    }(self)
    
    //challenge 1
    lazy var deleteAction: Action<NoteItem, Void> = { (service: NoteServiceType) in
        return Action { item in
            return service.delete(note: item)
        }
    }(self.noteService)
    
}

