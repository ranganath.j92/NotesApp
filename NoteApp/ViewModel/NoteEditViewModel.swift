//
//  NoteEditViewModel.swift
//  NoteApp
//
//  Created by apple on 4/12/18.
//  Copyright © 2018 Vibrant. All rights reserved.
//


import Foundation
import RxSwift
import Action

struct EditNoteViewModel {
    
    let itemTitle: String
    let itemDesc:String
    let onUpdate: Action<(String,String), Void>
    let onCancel: CocoaAction!
    let disposeBag = DisposeBag()
    let isReadOnly:Bool
    
    init(note: NoteItem, coordinator: SceneCoordinatorType, updateAction: Action<(String,String), Void>, cancelAction: CocoaAction? = nil,readOnly:Bool = false) {
        isReadOnly = readOnly
        itemTitle = note.nTitle
        itemDesc = note.nDesc
        onUpdate = updateAction
        onCancel = CocoaAction {
            if let cancelAction = cancelAction {
                cancelAction.execute(())
            }
            return coordinator.pop()
                .asObservable().map { _ in }
        }
        
        onUpdate.executionObservables
            .take(1)
            .subscribe(onNext: { _ in
                coordinator.pop()
            })
            .disposed(by: disposeBag)
    }
}
