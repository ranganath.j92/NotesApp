//
//  NoteService.swift
//  NoteApp
//
//  Created by apple on 4/12/18.
//  Copyright © 2018 Vibrant. All rights reserved.
//




import Foundation
import RealmSwift
import RxSwift
import RxRealm

struct NoteService: NoteServiceType {
    
    init() {
        // create a few default notes
        do {
            let realm = try Realm()
            if realm.objects(NoteItem.self).count == 0 {
                [(title:"Hi",desc:"How are you?"),(title:"Hello",desc:"How do you do?")].forEach {
                    self.createNote(title: $0.title, desc: $0.desc)
                }
            }
        } catch _ {
        }
    }
    
    fileprivate func withRealm<T>(_ operation: String, action: (Realm) throws -> T) -> T? {
        do {
            let realm = try Realm()
            return try action(realm)
        } catch let err {
            print("Failed \(operation) realm with error: \(err)")
            return nil
        }
    }
    
    @discardableResult
    func createNote(title: String,desc:String) -> Observable<NoteItem> {
        let result = withRealm("creating") { realm -> Observable<NoteItem> in
            let note = NoteItem()
            note.nTitle = title
            try realm.write {
                note.nId = (realm.objects(NoteItem.self).max(ofProperty: "nId") ?? 0) + 1
                realm.add(note)
            }
            return .just(note)
        }
        return result ?? .error(NoteServiceError.creationFailed)
    }
    
    @discardableResult
    func delete(note: NoteItem) -> Observable<Void> {
        let result = withRealm("deleting") { realm-> Observable<Void> in
            try realm.write {
                realm.delete(note)
            }
            return .empty()
        }
        return result ?? .error(NoteServiceError.deletionFailed(note))
    }
    
    @discardableResult
    func update(note: NoteItem, title: String,desc:String) -> Observable<NoteItem> {
        let result = withRealm("updating title") { realm -> Observable<NoteItem> in
            try realm.write {
                note.nTitle = title
                note.nDesc = desc
            }
            return .just(note)
        }
        return result ?? .error(NoteServiceError.updateFailed(note))
    }
    
    func notes() -> Observable<Results<NoteItem>> {
        let result = withRealm("getting notes") { realm -> Observable<Results<NoteItem>> in
            let realm = try Realm()
            let notes = realm.objects(NoteItem.self)
            return Observable.collection(from: notes)
        }
        return result ?? .empty()
    }
    
    //challenge 2
    func numberOfNotes() -> Observable<Int> {
        let result = withRealm("number of notes") { realm -> Observable<Int> in
            let notes = realm.objects(NoteItem.self)
            return Observable.collection(from: notes)
                .map { $0.count }
        }
        return result ?? .empty()
    }
    
    //challenge2
    func statistics() -> Observable<NoteStatistics> {
        let result = withRealm("getting statistics") { realm -> Observable<NoteStatistics> in
            let notes = realm.objects(NoteItem.self)
            let todoNotes = notes.filter("checked != nil")
            return .combineLatest(
                Observable.collection(from: notes)
                    .map { $0.count },
                Observable.collection(from: todoNotes)
                    .map { $0.count }) { all, done in
                        (todo: all - done, done: done)
            }
        }
        return result ?? .empty()
    }
}

