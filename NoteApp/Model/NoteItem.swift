//
//  NoteItem.swift
//  NoteApp
//
//  Created by apple on 4/12/18.
//  Copyright © 2018 Vibrant. All rights reserved.
//


import Foundation
import RealmSwift
import RxDataSources

class NoteItem: Object {
    @objc dynamic var nId: Int = 0
    @objc dynamic var nTitle: String = ""
    @objc dynamic var nDesc: String = ""
    @objc dynamic var added: Date = Date()
    
    
    override class func primaryKey() -> String? {
        return "nId"
    }
}

extension NoteItem: IdentifiableType {
    var identity: Int {
        return self.isInvalidated ? 0 : nId
    }
}

