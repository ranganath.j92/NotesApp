//
//  NoteListVC.swift
//  NoteApp
//
//  Created by apple on 4/12/18.
//  Copyright © 2018 Vibrant. All rights reserved.
//

import UIKit
import RxSwift
import RxDataSources
import Action
import NSObject_Rx

class NoteListVC: UIViewController, BindableType {
    
    @IBOutlet var tableView: UITableView!
    @IBOutlet var newNoteButton: UIBarButtonItem!
    
    var viewModel: NotesViewModel!
    var dataSource: RxTableViewSectionedAnimatedDataSource<NoteSection>!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 60
        tableView.tableFooterView = UIView(frame: CGRect.zero)
        
        configureDataSource()
        self.navigationItem.title = "Notes"
    }
    
    func bindViewModel(to model: NotesViewModel) {
        viewModel = model
        loadViewIfNeeded()
        bindViewModel()
    }
    
    func bindViewModel() {
        viewModel.sectionedItems
            .bind(to: tableView.rx.items(dataSource: dataSource))
            .disposed(by: self.rx.disposeBag)
        
        newNoteButton.rx.action = viewModel.onCreateNote()
        
        tableView.rx.itemSelected
            .do(onNext: { [unowned self] indexPath in
                self.tableView.deselectRow(at: indexPath, animated: false)
            })
            .map { [unowned self] indexPath in
                try! self.dataSource.model(at: indexPath) as! NoteItem
            }
            .subscribe(viewModel.readAction.inputs)
            .disposed(by: self.rx.disposeBag)
        
        
        //challenge 1
        tableView.rx.itemDeleted
            .map { [unowned self] indexPath in
                try! self.tableView.rx.model(at: indexPath)
            }
            .subscribe(viewModel.deleteAction.inputs)
            .disposed(by: self.rx.disposeBag)
    }
    
    fileprivate func configureDataSource() {
        dataSource = RxTableViewSectionedAnimatedDataSource<NoteSection>(
            configureCell: {
                [weak self] dataSource, tableView, indexPath, item in
                let cell = tableView.dequeueReusableCell(withIdentifier: "NoteListCell", for: indexPath) as! NoteListCell
                
                if let strongSelf = self {
                    let action = CocoaAction {_ in
                        return strongSelf.viewModel.editAction.execute(item).map{_ in}
                    }
                    cell.configure(with: item, action: action)
                }
                return cell
            },
         
            
            canEditRowAtIndexPath: { _,_ in true }
        )
        
    }
    
}

