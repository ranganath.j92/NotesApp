//
//  NoteViewVC.swift
//  NoteApp
//
//  Created by apple on 4/12/18.
//  Copyright © 2018 Vibrant. All rights reserved.
//




import UIKit
import RxSwift
import RxCocoa
import Action
import NSObject_Rx

class NoteViewVC: UIViewController, BindableType {
    
    @IBOutlet var descView: UITextView!
    @IBOutlet var titleField: UITextField!
    @IBOutlet var okButton: UIButton!
    @IBOutlet var cancelButton: UIButton!
    
    var viewModel: EditNoteViewModel!
    var isReadOnly:Observable<Bool>!
    
    func bindViewModel() {
        titleField.text = viewModel.itemTitle
        descView.text = viewModel.itemDesc
        cancelButton.rx.action = viewModel.onCancel
        isReadOnly = Observable.just(viewModel.isReadOnly)
        isReadOnly.subscribe(onNext: { [weak self] value in
                self?.titleField.isUserInteractionEnabled = !value
                self?.descView.isUserInteractionEnabled = !value
                self?.cancelButton.isHidden = value
                self?.okButton.isHidden = value
        }).disposed(by: self.rx.disposeBag)
        
        let maxLength = 300
        descView.rx.text.orEmpty
            .subscribe(onNext: { [weak self] in
                if $0.count > maxLength{
                    self?.descView.text = String($0[..<maxLength])
                    self?.descView.resignFirstResponder()
                }
            })
            .disposed(by: self.rx.disposeBag)
        
        
        let observable =  Observable.combineLatest(titleField.rx.text.orEmpty.asObservable(), descView.rx.text.orEmpty.asObservable(), resultSelector: {
            lastLeft, lastRight in
            (lastLeft ,lastRight)
        })
        
        okButton.rx.tap
            .withLatestFrom(observable)
            .subscribe(viewModel.onUpdate.inputs)
            .disposed(by: self.rx.disposeBag)
    }
    
    func bindViewModel(to model: EditNoteViewModel) {
        viewModel = model
        loadViewIfNeeded()
        bindViewModel()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        titleField.becomeFirstResponder()
    }
    
    override func viewDidLoad() {
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "back_icn"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(backButtonPressed))
//            = UIBarButtonItem(title: "", style: .plain, target: self, action: #selector(backButtonPressed))
    }
    
    @objc func backButtonPressed(){
        cancelButton.sendActions(for: .touchUpInside)
    }
}


