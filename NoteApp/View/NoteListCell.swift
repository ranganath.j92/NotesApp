//
//  NoteListCell.swift
//  NoteApp
//
//  Created by apple on 4/12/18.
//  Copyright © 2018 Vibrant. All rights reserved.
//

import UIKit
import Action
import RxSwift

class NoteListCell: UITableViewCell {
    
    @IBOutlet var title: UILabel!
    @IBOutlet var button: UIButton!
    var disposeBag = DisposeBag()
    
    func configure(with item: NoteItem, action: CocoaAction) {
        button.rx.action = action
        
        item.rx.observe(String.self, "nTitle")
            .subscribe(onNext: { [weak self] title in
                self?.title.text = title
            })
            .disposed(by: disposeBag)

    }
    
    override func prepareForReuse() {
        button.rx.action = nil
        disposeBag = DisposeBag()
        super.prepareForReuse()
    }
}

