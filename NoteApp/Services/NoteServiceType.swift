//
//  NoteServiceType.swift
//  NoteApp
//
//  Created by apple on 4/12/18.
//  Copyright © 2018 Vibrant. All rights reserved.
//


import Foundation
import RxSwift
import RealmSwift

enum NoteServiceError: Error {
    case creationFailed
    case updateFailed(NoteItem)
    case deletionFailed(NoteItem)
    case toggleFailed(NoteItem)
}

//challenge2
typealias NoteStatistics = (todo: Int, done: Int)

protocol NoteServiceType {
    @discardableResult
    func createNote(title: String,desc:String) -> Observable<NoteItem>
    
    @discardableResult
    func delete(note: NoteItem) -> Observable<Void>
    
    @discardableResult
    func update(note: NoteItem, title: String,desc:String) -> Observable<NoteItem>
    
    func notes() -> Observable<Results<NoteItem>>
}
